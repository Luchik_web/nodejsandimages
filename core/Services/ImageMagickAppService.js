var imagemagick, fs,
    ImageMagickNativeAppService,
    LOG_ITERATIONS;

imagemagick = require('imagemagick');
fs = require('fs');

LOG_ITERATIONS = (function() {
    var _startItemsCount = 0,
        _iterations = 0,
        _startTime;

    return {
        set: function(iterations, startTime) {
            _startItemsCount = iterations;
            _iterations = iterations;
            _startTime = startTime;
            console.log('ImageMagick start processing : ' + (new Date().getTime() - _startTime));
        },
        check: function() {
            _iterations--;
            if (_iterations < 0) {
                console.log('ImageMagick: All ' + _startItemsCount + ' images were processed for '
                    + (new Date().getTime() - _startTime) + ' ms');
            }
        }
    }
})();

ImageMagickNativeAppService = {
    run: function(maxIteration, startTime) {
        var i, steps, percentage, path;
        i = maxIteration;
        path = 'data/imginit/girl1_beige1.jpg';

        steps = 3;
        percentage = 70;
        LOG_ITERATIONS.set(maxIteration, startTime);

        for (i = 0; i <= maxIteration; i++) {
            this.resizeImg(path, percentage, steps, i);
        }
    },
    resizeImg: function(path, percentage, steps, i) {
        fs.readFile(path, function(err, buffer) {
            if (err) {
                throw err;
            }
            var pathNew;
            pathNew = 'data/img/1/girl1_beige1_resized' + i + '.jpg';
            _resizeImg(buffer, pathNew, percentage, steps);
        });
    }
}

function _resizeImg(buffer, pathNew, percentage, step) {
    imagemagick.resize({
        srcData: buffer,
        width: percentage + '%',
        format: 'jpg',
        quality: percentage
    }, function(err, stdout, stderr) {
        if (err) {
            throw err;
        }

        if (0 < step) {
            _resizeImg(stdout, pathNew, percentage, step - 1);
        } else {
            fs.writeFile(pathNew, stdout, 'binary', function(err) {
                if (err) {
                    throw err;
                }
                LOG_ITERATIONS.check();
            });
        }
    });
}

exports.service = ImageMagickNativeAppService;