var imagemagick, fs,
    ImageMagickNativeAppService,
    LOG_ITERATIONS;

imagemagick = require('../../node_modules/imagemagick-native/');
fs = require('fs');

LOG_ITERATIONS = (function() {
    var _startItemsCount = 0,
        _iterations = 0,
        _startTime;

    return {
        set: function(iterations, startTime) {
            _startItemsCount = iterations;
            _iterations = iterations;
            _startTime = startTime;
            console.log('ImageMagick Native start processing : ' + (new Date().getTime() - _startTime));
        },
        check: function() {
            _iterations--;
            if (_iterations < 0) {
                console.log('ImageMagick Native: All ' + _startItemsCount + ' images were processed for '
                    + (new Date().getTime() - _startTime) + ' ms');
            }
        }
    }
})();

//console.log(imagemagick);

ImageMagickNativeAppService = {
    run: function(maxIteration, startTime) {
        var i, steps, percentage, path;

        path = 'data/imginit/girl1_beige1.jpg';
        steps = 3;
        percentage = 70;

        LOG_ITERATIONS.set(maxIteration, startTime);

        for (i = 0; i <= maxIteration; i++) {
            this.resizeImg(path, percentage, steps, i);
        }
    },
    resizeImg: function(path, percentage, steps, i) {

        fs.readFile(path, function(err, buffer) {
            if (err) {
                throw err;
            }

            var dimensions, stdout, pathNew;
            dimensions = imagemagick.identify({
                srcData: buffer
            });
            stdout = _resizeImg(buffer, dimensions.width, dimensions.height, percentage, steps);

            pathNew = 'data/img/2/girl1_beige1_resized' + i + '.jpg';
            fs.writeFile(pathNew, stdout, function(err) {
                if (err) {
                    throw err;
                }
                LOG_ITERATIONS.check();
            });
        });
    }
}

function _resizeImg(buffer, width, height, percentage, step) {
    if (0 > step) {
        return buffer;
    }

    var stdout;
    width *= percentage / 100;
    height *= percentage / 100;
    stdout = imagemagick.convert({
        srcData: buffer,
        width: width,
        height: height,
        resizeStyle: 'aspectfit',
        quality: percentage,
        format: 'JPEG'
    });
    stdout = _resizeImg(stdout, width, height, percentage, step - 1);
    return stdout;
}

exports.service = ImageMagickNativeAppService;