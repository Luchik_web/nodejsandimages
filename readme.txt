---------- NODE ----------

NODE:
1) INSTALL node.js

---------- IMAGEMAGICK ----------
IMAGEMAGICK:
1) add imagemagick to node.js
	npm install imagemagick
	
---------- IMAGEMAGICK-NATIVE ----------
GIT WITH INSTALL GUIDE
https://npmjs.org/package/imagemagick-native

WINDOWS INSTALLATION:
Proposed install guide:
* Windows (tested on Windows 7 x64)
1) Install Python 2.7.3 (http://www.python.org/ftp/python/2.7.3/python-2.7.3.amd64.msi) only 2.7.3 nothing else works quite right!
   If you use Cygwin ensure you don't have Python installed in Cygwin setup as there will be some confusion about what version to use.
2) Install Visual Studio C++ 2010 Express (http://download.microsoft.com/download/1/D/9/1D9A6C0E-FC89-43EE-9658-B9F0E3A76983/vc_web.exe) (64-bit only) 
3) Install Windows 7 64-bit SDK (http://www.microsoft.com/en-us/download/details.aspx?id=8279)
4) Install Imagemagick dll binary x86 Q16 or x64 Q16(can be found on http://www.imagemagick.org/script/binary-releases.php OR by direct link for v6.8.8: http://www.imagemagick.org/download/binaries/ImageMagick-6.8.8-4-Q16-x64-dll.exe) and check for libraries and includes during install.
5) Install imagemagick-native module for node.js
	npm install imagemagick-native
	
Our solution (as proposed does not work):
1) Install Python 2.7.3 (it was mentioned that other versions will not work)
2) Install Visual Studio C++ 2012/2013 Express 
3) Install Imagemagick dll binary x64 Q16 (http://www.imagemagick.org/script/binary-releases), we have ImageMagick-6.8.8-Q16 (x64)
3) Go to folder where the module should be installed to
4) Run install imagemagick-native with force (to save all files, when it will be an error all folder will be removed without force)
	npm install imagemagick-native --force
5) We will get an error:
	 error C3861: 'snprintf': identifier not found
snprintf - is depricated command in Windows, to define it in installer (if we will open directly from git file imagemagic-native-master\src\imagemagick.cc) developers used (!The string will not exist in compiled file)
	#if _MSC_VER
	#define snprintf _snprintf
	#endif
the definer (as we think) is uset to define if we are using windows or not, but _MSC_VER is not defined (for VS2012/VS2013 windows 7/8)
Now all we need is to define in our file \node_modules\imagemagick-native\src\imagemagick.cc
	#define snprintf _snprintf
6) Run rebuild
	npm rebuild imagemagick-native
TADA!!!! It works! :) 

LINUX INSTALLATION:
* Linux / Mac (tested with ImageMagick 6.7.7 on CentOS6 and MacOS10.7, Ubuntu12.04)
1) Install Imagemagick (http://www.imagemagick.org/) with headers before installing this module. 
	brew install imagemagick --build-from-source
		OR
	sudo yum install ImageMagick-c++ ImageMagick-c++-devel
		OR
	sudo apt-get install libmagick++-dev
2) Make sure you can find Magick++-config in your PATH. 
3) Install imagemagick-native module for node.js
	npm install imagemagick-native


	
	
RUN APPLICATION:
1) run application
	node app.js