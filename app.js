var server = require("./infrastructure/server");
var router = require("./infrastructure/router");
var requestHandlers = require("./infrastructure/requestHandlers");

var handle = {}
handle["/"] = requestHandlers.start;
handle["/start"] = requestHandlers.start;
handle["/upload"] = requestHandlers.upload;
handle["/show"] = requestHandlers.show;

var ImageMagickNativeAppService = require("./core/Services/ImageMagickNativeAppService");
var ImageMagickAppService = require("./core/Services/ImageMagickAppService");

var iterationsCount = 100;

//ImageMagickNativeAppService.service.run(iterationsCount, new Date().getTime());
ImageMagickAppService.service.run(iterationsCount, new Date().getTime());

server.start(router.route, handle);